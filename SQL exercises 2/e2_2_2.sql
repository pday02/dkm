
select r.party , sum(ukvotes) as votes
from ukresults r
group by r.party
having sum(ukvotes) > (select sum(r2.ukvotes) from ukresults r2 where r2.party = 'lib')
order by votes desc
;
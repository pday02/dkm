--For each constituency, find the constituency name and the winning party in that
--constituency. Order result rows by constituency name.

select c.ukarea , r.party
from ukconsts c, (select r1.party, r1.uknum from ukresults r1
					where r1.ukvotes = (select max(r2.ukvotes) 
										from ukresults r2
										where r2.uknum = r1.uknum)
				) r
	where c.uknum = r.uknum
	order by ukarea asc
; 